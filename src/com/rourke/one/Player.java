package com.rourke.one;

public abstract class Player{
	protected Hand hand;
	protected GameData data;
	protected GameOptions options;
	protected int score;
	protected int name;
	
	public Player(GameData data_in, GameOptions options_in, int name_in) {
		if(data_in == null || options_in == null)
			throw new IllegalArgumentException("data_in and options_in must not be null");
		
		data = data_in;
		options = options_in;

		int total_colors = options.getNumColors();
		if(options.wild4Enabled() || options.wildEnabled())
			total_colors++;

		hand = new Hand(total_colors, data.getNumValues());

		name = name_in;

		score = 0;
	}
	public abstract Card play(Card card_in_play);
	public abstract int chooseWildColor();
	public abstract boolean playDrawnCard();
	
	/*
	protected void constructor(GameData data_in, GameOptions options_in, int name_in){
		if(data_in == null || options_in == null)
			throw new NullPointerException();
		data = data_in;
		options = options_in;

		int total_colors = options.getNumColors();
		if(options.wild4Enabled() || options.wildEnabled())
			total_colors++;

		hand = new Hand(total_colors, data.getNumValues());

		name = name_in;

		score = 0;
	}
	*/
	
	public void draw(Card card){
		try{
			hand.insert(card);
		} catch(IllegalArgumentException e){
			throw e;
		}
	}
	
	public Card remove(Card card){
		try{
			return hand.remove(card);
		} catch(IllegalArgumentException e){
			throw e;
		}
	}
	
	public int numCardsOfColor(int color){ 
		try{
			return hand.numCardsOfColor(color); 
		} catch(IllegalArgumentException e){
			throw e;
		}
	}
	
	public int getNumCardsInHand(){ return hand.getNumCards(); }
	public int getScore(){ return score; }
	public int getName(){ return name; }
	public void setScore(int new_score){ score = new_score; }
	public void resetIter(){ hand.resetIter(); }
	public Card nextIter(){ return hand.nextIter(); }
}