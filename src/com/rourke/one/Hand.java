package com.rourke.one;

public class Hand{
	private class Node{
		Card value;
		Node next;
		int depth;
	}
	
	private class Iter{
		int color_index;
		int value_index;
		int depth_index;
	}
	
	private Node hand[][];
	private int color_counter[];
	int num_colors;
	int num_values;
	int num_cards;
	Iter iter = new Iter();
	
	public Hand(int colors, int values){
		if(colors < 0 || values < 0)
			throw new IllegalArgumentException("parameters must be positive");
		
		num_colors = colors;
		num_values = values;
		resetIter();
		hand = new Node[num_colors][num_values];
		color_counter = new int[num_colors];
		num_cards = 0;
		
		for(int i=0; i<num_colors; i++){
			color_counter[i] = 0;
			for(int j=0; j<num_values; j++)
				hand[i][j] = null;
		}
	}
	
	public void insert(Card card){
		if(card == null)
			throw new IllegalArgumentException("card must not be null");
		
		int color = card.getColor();
		int value = card.getValue();
		
		if(color>=num_colors || color<0 || value>=num_values || value<0)
			throw new ArrayIndexOutOfBoundsException();
		
		Node new_node = new Node();
		new_node.value = card;
		new_node.next = hand[color][value];
		if(hand[color][value] == null)
			new_node.depth = 0;
		else
			new_node.depth = hand[color][value].depth+1;
		
		hand[color][value] = new_node;
		color_counter[color]++;
		num_cards++;
	}
	
	public boolean cardInHand(Card card){
		if(card == null)
			throw new IllegalArgumentException("card must not be null");
		return cardInHand(card.getColor(), card.getValue());
	}
	
	public boolean cardInHand(int color, int value){
		if(color>=num_colors || color<0 || value>=num_values || value<0)
			throw new IllegalArgumentException("color and value must be in bounds of array");
		return hand[color][value] != null;
	}
	
	public Card remove(Card card){
		if(card == null)
			throw new IllegalArgumentException("card must not be null");
		return remove(card.getColor(), card.getValue());
	}
	
	public Card remove(int color, int value){
		if(color>=num_colors || color<0 || value>=num_values || value<0)
			throw new IllegalArgumentException("color and value must be within array bounds");
		
		Card card = null;
		if(hand[color][value] != null){
			card = hand[color][value].value;
			hand[color][value] = hand[color][value].next;
			
			color_counter[color]--;
			num_cards--;
		}
		return card;
	}
	
	public void resetIter(){
		iter.color_index = 0;
		iter.value_index = 0;
		iter.depth_index = 0;
	}
	
	public Card nextIter(){
		Card card = null;
		for(int i=iter.color_index; i<num_colors; i++){
			int j=0;
			if(i==iter.color_index)
				j=iter.value_index;
			for(; j<num_values; j++){
				if(hand[i][j] != null){
					if(hand[i][j].depth >= iter.depth_index){
						card = hand[i][j].value;
						iter.depth_index++;
						iter.color_index = i;
						iter.value_index = j;
						return card;
					} else
						iter.depth_index = 0;
				}
			}
		}
		return card;
	}
	
	public int getNumCards(){
		return num_cards;
	}
	
	public int numCardsOfColor(int color){
		if(color >= num_colors || color < 0)
			throw new IllegalArgumentException("bad color given");
		return color_counter[color];
	}
}