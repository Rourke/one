package com.rourke.one;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class PlayerBar {
	Context context;
	LinearLayout playerBar;
	boolean clockwise;
	final int arrowImage = R.drawable.ic_launcher;
	
	public PlayerBar(Context context){
		this.context = context;
		playerBar = (LinearLayout)((Activity)context).findViewById(R.id.playerbar);
		clockwise = true;
	}
	
	@SuppressLint("NewApi") //TODO fix this for api 8
	private void insertArrow(){
		ImageView arrow = new ImageView(context);
		arrow.setLayoutParams(new LinearLayout.LayoutParams(85, 85));
		arrow.setScaleType(ImageView.ScaleType.CENTER_CROP);
		arrow.setImageResource(arrowImage);
		
		if(clockwise)
			arrow.setRotation(0);
		else
			arrow.setRotation(180);
		
		playerBar.addView(arrow);
	}
	
	public int addPlayer(String name){
		PlayerView player = new PlayerView(context);
		player.setName(name);
		player.setLayoutParams(new LinearLayout.LayoutParams(85, 85));

		int id = playerBar.getChildCount() / 2;
		
		if(id == 0)
			insertArrow();
		playerBar.addView(player);
		insertArrow();
		
		return id;
	}
	
	private int getIndex(int playerId){
		return (playerId * 2) + 1;
	}
	
	public void updateScore(int playerId, int score){
		int index = getIndex(playerId);
		
		if(index < 0 || index >= playerBar.getChildCount())
			throw new IllegalArgumentException("Bad ID given. 0 <= ID < # of children in the player bar");
		
		PlayerView player = (PlayerView)playerBar.getChildAt(index);
		
		player.setScore(score);
	}
	
	public void updateNumCards(int playerId, int numCards){
		int index = getIndex(playerId);
		
		if(index < 0 || index >= playerBar.getChildCount())
			throw new IllegalArgumentException("Bad ID given. 0 <= ID < # of children in the player bar");
		
		PlayerView player = (PlayerView)playerBar.getChildAt(index);
		
		player.setNumCards(numCards);
	}
	
	@SuppressLint("NewApi") //TODO fix for api 8
	public void setClockwise(boolean clockwise){
		if(this.clockwise != clockwise){
			this.clockwise = clockwise;
			
			ImageView image;
			
			for(int i=0; i<playerBar.getChildCount(); i = i+2){
				image = (ImageView)playerBar.getChildAt(i); 
				image.setRotation(image.getRotation()+180);
			}
		}
	}
	
	public String getName(int playerId){
		int index = getIndex(playerId);
		return ((PlayerView)playerBar.getChildAt(index)).getName();
	}
}
