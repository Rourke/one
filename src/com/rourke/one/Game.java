package com.rourke.one;

import java.util.concurrent.Semaphore;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class Game implements Runnable{
	private Deck draw_pile;
	private Deck discard_pile;
	private PlayerList players;
	private GameData data;
	private GameOptions options;
	private final Player human;
	private Player first_player;
	private int game_state;
	private Boolean running = true;;
	private Boolean paused = false;
	Semaphore semaphore;
	Handler handler;
	PlayerBar playerBar;
	
	public final int NEW_GAME = 0;
	public final int NEW_ROUND = 1;
	public final int CPU_TURN = 2;
	public final int PLAYER_TURN = 3;
	public final int END_ROUND = 4;
	public final int END_GAME = 5;
	
	
	public Game(final GameOptions options, Context context, PlayerBar playerBar, GridView gridView, Handler handler, CardView topCard){ 
		if(options == null || context == null || playerBar == null || gridView == null || handler == null)
			throw new IllegalArgumentException("all arguments must not be null");
		this.options = options;
		this.playerBar = playerBar;
		this.handler = handler;
		discard_pile = new Deck();
		draw_pile = new Deck();
		players = new PlayerList();
		data = new GameData(options);
		
		topCard.setOptions(options);
		topCard.setData(data);
		
		running = true;
		paused = false;
	
		createCards();
		draw_pile.shuffle();
	
		//create players	
		int id = playerBar.addPlayer("Player");
		human  = new HumanPlayer(data, options, id);
		players.addPlayer(human);
		for(int i=1; i<=options.getNumPlayers()-1; i++){
			id = playerBar.addPlayer("CPU" + Integer.toString(i));
			players.addPlayer(new CPUPlayer(data, options, id));
		}
		
		game_state = NEW_GAME;
		
		semaphore = new Semaphore(1);
		
		gridView.setAdapter(new CardAdapter(context, human, options, data));
		gridView.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> parent, View v, int position, long id){
				try{
					semaphore.acquire();
					try{
						if(game_state == PLAYER_TURN){
							CardView cardView = (CardView)v;
							int color = cardView.getColor();
							int value = cardView.getValue();
							Card card = new Card(color, value);
							if(checkCard(card)){
								playCard(human.remove(card));
								if(color != data.getWildColorId())
									endTurn(human);
							} else {
								if(!(value == data.getWild4Id() && options.wild4Enabled())) //wild draw 4 has it's own toast
									toast("Chosen card is not playable");
							}
						}
					} finally{
						semaphore.release();
					}
				} catch(InterruptedException ie){
					
				}
			}
		}); 
	}
	
	public void reneg(){
		try{
			semaphore.acquire();
			try{
				if(game_state == PLAYER_TURN){
					// draw card. check if playable. player chooses whether or not to play it
					Card drawn_card = drawCard(human);
					logEvent(null, playerToString(human), "Draws a card");
					if(checkCard(drawn_card)){ 
						Bundle b = new Bundle();
						b.putInt(GameActivity.TYPE_KEY, GameActivity.DRAWN_CARD_DIALOG);
						b.putInt(GameActivity.COLOR_KEY, drawn_card.getColor());
						b.putInt(GameActivity.VALUE_KEY, drawn_card.getValue());
						b.putParcelable(GameActivity.OPTIONS_KEY, options);
						b.putParcelable(GameActivity.DATA_KEY, data);
						
						sendMessage(b);
						//human.remove(drawn_card);
						//playCard(drawn_card);
					} else
						endTurn(human);
				}
			} finally{
				semaphore.release();
			}
		} catch(InterruptedException ie){
			
		}
	}
	
	public void playDrawnCard(boolean status, int color, int value){
		try{
			semaphore.acquire();
			try{
				if(game_state == PLAYER_TURN){
					if(status){
						Card card = new Card(color, value);
						human.remove(card);
						playCard(card);
					}
					if(color != data.getWildColorId())
						endTurn(human);
				}
			} finally{
				semaphore.release();
			}
		} catch(InterruptedException ie){
			
		}
	}
	
	public void chooseWildColor(int color){
		try{
			semaphore.acquire();
			try{
				if(game_state == PLAYER_TURN){
					finishPlayWild(human, color);
					endTurn(human);
				}
			} finally{
				semaphore.release();
			}
		} catch(InterruptedException ie){
			
		}
	}
	
	private void createCards(){
		// TODO: get rid of the hardcoded conditionals? would mean additional GameOption attributes
		for(int i=0; i<options.getNumColors(); i++){
			// create number cards
			for(int j=0; j<options.getNumNumbers(); j++){
				draw_pile.push(new Card(i,j));
				if(j != 0)
					draw_pile.push(new Card(i,j));
			}
	
			//create draw2 cards
			for(int j=0; j<2 && options.draw2Enabled(); j++)
				draw_pile.push(new Card(i,data.getDraw2Id()));
	
			//create reverse cards
			for(int j=0; j<2 && options.reverseEnabled(); j++)
				draw_pile.push(new Card(i, data.getReverseId()));
	
			//create skip cards
			for(int j=0; j<2 && options.skipEnabled(); j++)
				draw_pile.push(new Card(i, data.getSkipId()));
		}
		//create wild cards
		for(int i=0; i<4 && options.wildEnabled(); i++)
			draw_pile.push(new Card(data.getWildColorId(), data.getWildId()));
		//create wild draw 4 cards
		for(int i=0; i<4 && options.wild4Enabled(); i++)
			draw_pile.push(new Card(data.getWildColorId(), data.getWild4Id()));
	}
	
	public void setRunning(boolean running){
		synchronized(this.running){
			this.running = running;
		}
	}
	
	public void setPaused(boolean pausedState){
		synchronized(paused){
			boolean temp = paused;
			paused = pausedState;
			if(temp && !pausedState) //only notify if game was paused and is being resumed
				paused.notify();
		}
	}
	
	private void endTurn(Player current){
		if(current == null)
			throw new IllegalArgumentException("current player must not be null");
		
		notifyGrid();
		updateNumCards(current.getName(), current.getNumCardsInHand());
		
		if(current.getNumCardsInHand() == 0)
			game_state = END_ROUND;
		else{
			Player next = players.nextPlayer();
			if(next == human)
				game_state = PLAYER_TURN;
			else
				game_state = CPU_TURN;
		}
	}
	
	private void notifyGrid() {
		Bundle b = new Bundle();
		b.putInt(GameActivity.TYPE_KEY, GameActivity.HAND_GRID_MSG);
		sendMessage(b);
	}
	
	private boolean checkCard(Card card_played){
		if(card_played == null)
			throw new IllegalArgumentException("No card given to check");
		
		boolean match = false;
		Card card_in_play = discard_pile.peek();
		
		//match if colors or values are the same, or a wild is played
		if((card_played.getColor() == card_in_play.getColor()) || (card_played.getValue() == card_in_play.getValue()) ||
			(card_played.getColor() == data.getWildColorId()) || (card_in_play.getColor() == data.getWildColorId())){
	
				//wild draw 4 only playable if no cards of the current color are in the player's hand
				if(card_played.getValue() == data.getWild4Id() && options.wild4Enabled()){
					int num_cards = players.getCurrent().numCardsOfColor(card_in_play.getColor());
					match = (num_cards == 0);
					if(!match){
						toast("Unable to play Wild Draw 4. Cards with matching color exist");
						//TODO toast of this: printf("\nWild Draw 4 is only playable if you have no cards matching the color of the card in play\n");
					}
				} else
					match = true;
		}
		return match;
	}
	
	private void discard(Card card){
		if(card == null)
			throw new IllegalArgumentException();
		
		discard_pile.push(card);
		
		Bundle bundle = new Bundle();
		bundle.putInt(GameActivity.TYPE_KEY, GameActivity.TOP_CARD_MSG);
		bundle.putInt(GameActivity.COLOR_KEY, card.getColor());
		bundle.putInt(GameActivity.VALUE_KEY, card.getValue());
		
		sendMessage(bundle);
	}
	
	private void playCard(Card card){
		if(card == null)
			throw new NullPointerException("No card given to be played");
		
		int value = card.getValue();
	
		if(discard_pile.peek().getValue() == data.getPlaceholderId()) //get rid of wild card color placeholder
			discard_pile.pop();
		discard(card); // place down card
		
		Player current_player = players.getCurrent();
		logEvent(card, playerToString(current_player), "Played");
	
		// perform actions for special cards
		if(value >= options.getNumNumbers()){ //only check non-number cards. saves time, but somewhat pointless
			if(value == data.getDraw2Id() && options.draw2Enabled()){
				Player target = players.nextPlayer();
				playDrawX(target, 2);
			} else if(value == data.getSkipId() && options.skipEnabled()){
				playSkip();
			} else if(value == data.getReverseId() && options.reverseEnabled()){
				playReverse();
			} else if(value == data.getWildId() && options.wildEnabled()){
				playWild(current_player);
			} else if(value == data.getWild4Id() && options.wild4Enabled()){
				playWild4(current_player);
			}
		}
	}
	
	private Card drawCard(Player player){
		if(player == null)
			throw new NullPointerException("No player given");
		
		Card drawn_card = draw_pile.pop();
		if(drawn_card == null){ //no cards in the draw pile
			//take the card in play off of the discard pile
			Card card_in_play = discard_pile.pop(); 
	
			//move all cards in the discard pile to the draw pile, shuffle
			Deck temp = draw_pile;
			draw_pile = discard_pile;
			discard_pile = temp;
			draw_pile.shuffle();
	
			//put the card in play back on the discard pile
			discard_pile.push(card_in_play); 
	
			//try to draw again. don't do anything if draw pile still empty
			drawn_card = draw_pile.pop();
			if(drawn_card != null)
				player.draw(drawn_card);
		} else
			player.draw(drawn_card);
			
		return drawn_card;
		
	}
	
	private void playWild(Player player){
		if(player == null)
			throw new IllegalArgumentException("No player given");
		if(player == human){
			Bundle b = new Bundle();
			b.putInt(GameActivity.TYPE_KEY, GameActivity.WILD_DIALOG);
			b.putParcelable(GameActivity.OPTIONS_KEY, options);
			sendMessage(b);
		} else {
			//get a valid color
			int color_chosen = player.chooseWildColor();
			finishPlayWild(player, color_chosen);
		}
	}
		
	private void finishPlayWild(Player player, int color){
		if(player == null)
			throw new IllegalArgumentException("player must not be null");
		if(!(color >= 0 && color < options.getNumColors()))
			throw new IllegalArgumentException("invalid color given");
		
		Card card = new Card(color, data.getPlaceholderId());
		logEvent(card, playerToString(player), "Wild color");
	
		//put a placeholder card on the discard pile
		discard(card);
	}
	
	private void playDrawX(Player target, int x){
		if(target == null)
			throw new IllegalArgumentException("No target given");
		if(x < 0)
			throw new IllegalArgumentException("x must be positive");
		
		for(int i=0; i < x; i++)
			drawCard(target);
		
		logEvent(null, playerToString(target), "Draws" + Integer.toString(x) + "cards");
	}
	
	private void playReverse(){
		if(options.getNumPlayers() > 2){
			players.reverse();

			logEvent(null, "", "Direction reversed");
			
			Bundle b = new Bundle();
			b.putInt(GameActivity.TYPE_KEY, GameActivity.PLAYER_BAR_MSG);
			b.putBoolean(GameActivity.CLOCKWISE_KEY, players.isClockwise());
			sendMessage(b);
		}
		else
			playSkip();
	}
	
	private void playSkip(){
		Player skipped = players.nextPlayer();

		logEvent(null, playerToString(skipped), "Has been skipped");
	}
	
	private void playWild4(Player player){
		if(player == null)
			throw new IllegalArgumentException("No player given");
		
		playDrawX(players.nextPlayer(), 4);
		playWild(player);
	}
	
	private String playerToString(Player player){
		if(player == null)
			throw new IllegalArgumentException("player must not be null");
		int id = player.getName();
		return playerBar.getName(id);
	}
	
	private Player drawForFirstPlayer(){
		Player first = null;
		int val;
		int col;
		int max = 0;
		
		while(first == null){
			for(int i=0; i<options.getNumPlayers(); i++){
				col = draw_pile.peek().getColor();
				val = draw_pile.peek().getValue();
				logEvent(new Card(col, val), playerToString(players.getCurrent()), "Drew");
				draw_pile.shuffle();
				if(val >= 0 && val < options.getNumNumbers() && val > max){
					max = val;
					first = players.getCurrent();
				} else if(val >= 0 && val < options.getNumNumbers() && val == max){
					first = null; //multiple cards of same max value.. have everyone draw again
				}
				players.nextPlayer();
			}
		}
		
		logEvent(null, playerToString(first), "Goes first");
		
		return first;
	}
	
	private void newGame(){
		Player current = players.getCurrent();
		//reset scores
		for(int i=0; i<options.getNumPlayers(); i++){
			current.setScore(0);
			updateScore(current.getName(), current.getScore());
			current = players.nextPlayer();
		}
		
		//update state
		game_state = NEW_ROUND;
	}
	
	private void updateScore(int name, int score) {
		Bundle b = new Bundle();
		b.putInt(GameActivity.TYPE_KEY, GameActivity.PLAYER_BAR_MSG);
		b.putInt(GameActivity.PLAYER_KEY, name);
		b.putInt(GameActivity.SCORE_KEY, score);
		
		sendMessage(b);
	}
	
	private void newRound(){
		//put all cards in draw _pile
		Card temp;
		Player current;
		//collect potential leftover cards from players
		for(int i = 0; i<options.getNumPlayers(); i++){
			current = players.getCurrent();
			current.resetIter();
			while((temp = current.nextIter()) != null){
				if((temp = current.remove(temp)) != null)
					draw_pile.push(temp);
			}
		}
		//collect cards in discard _pile
		while((temp = discard_pile.pop()) != null){
			if(temp.getValue() != data.getPlaceholderId()) //only top card will ever be placeholder.. better safe than sorry?
				draw_pile.push(temp);
		}
		
		//clear log
		logEvent(null, null, null);
		
		//all cards are now in draw pile. shuffle
		draw_pile.shuffle();
	
		//deal cards
		for(int i=0; i<7; i++){ 
			for(int j=0; j<options.getNumPlayers(); j++){
				current = players.getCurrent();
				current.draw(draw_pile.pop());
				updateNumCards(current.getName(), current.getNumCardsInHand()); 
				players.nextPlayer();
			}
		}
		
		notifyGrid();
	
		//set the provided player to go first and direction of play to clockwise
		//get first player
		if(first_player == null){ //TODO add option to draw for first player every time. and clockwise of winner
			first_player = drawForFirstPlayer();
			players.reset(first_player);
		} else{
			//first person to play is player clockwise of person who went first last
			players.reset(first_player);
			first_player = players.nextPlayer();
		}
	
		//flip first card. wild draw 4 not allowed
		int value;
		//wild draw 4
		while(draw_pile.peek().getValue() == data.getWild4Id() && options.wild4Enabled())
			draw_pile.shuffle();
		temp = draw_pile.pop();
		value = temp.getValue();
		discard(temp);
		logEvent(temp, "", "First card");
		
		//draw 2
		if(value == data.getDraw2Id() && options.draw2Enabled()){
			playDrawX(players.getCurrent(), 2);
			players.nextPlayer();
		} else {
			//skip
			if(value == data.getSkipId() && options.skipEnabled())
				playSkip();
			else {
				//reverse
				if(value == data.getReverseId() && options.reverseEnabled()){
					playReverse();
					players.nextPlayer();
				} else {
					//wild
					if(value == data.getWildId() && options.wildEnabled())
						playWild(players.getCurrent());
				}
			}
		}
		
		if(first_player == human)
			notifyGrid(); //in case any cards were drawn
		
		//update game state
		if(players.getCurrent() == human)
			game_state = PLAYER_TURN;
		else
			game_state = CPU_TURN;
	}
	
	private void updateNumCards(int name, int numCardsInHand) {
		Bundle b = new Bundle();
		b.putInt(GameActivity.TYPE_KEY, GameActivity.PLAYER_BAR_MSG);
		b.putInt(GameActivity.PLAYER_KEY, name);
		b.putInt(GameActivity.NUM_CARDS_KEY, numCardsInHand);
		
		sendMessage(b);
	}
	
	private void cpuTurn(){
		Player current = players.getCurrent();
		Card card = current.play(discard_pile.peek());
		
		if(card != null)
			playCard(card);
		else {
			// draw card. check if playable. player chooses whether or not to play it
			Card drawn_card = drawCard(current);
			
			logEvent(null, playerToString(current), "Draws a card");

			if(checkCard(drawn_card)){
				if(current.playDrawnCard()){
					current.remove(drawn_card);
					playCard(drawn_card);
				}
			}
		}
		endTurn(current);
	}
	
	private void endRound(){
		Player current = players.getCurrent();
		Player winner = null;
		
		//find winner
		for(int i=0; i<options.getNumPlayers() && winner == null; i++){
			if(current.getNumCardsInHand() == 0)
				winner = current;
			current = players.nextPlayer();
		}
		
		// calc scores
		Card card;
		int player_score;
		for(int i=0; i<options.getNumPlayers(); i++){
			player_score = 0;
			current.resetIter();
			while((card = current.nextIter()) != null){
				if((card.getColor() == data.getWildColorId()) && (options.wild4Enabled() || options.wildEnabled()))
					player_score += 50;
				else {
					if((card.getValue() == data.getDraw2Id() && options.draw2Enabled()) ||
						(card.getValue() == data.getSkipId() && options.skipEnabled()) ||
						(card.getValue() == data.getReverseId() && options.reverseEnabled())){
							player_score += 20;
					} else
						player_score += card.getValue();
				}
				draw_pile.push(current.remove(card));
	
				current.resetIter(); //I don't like this much.. Required for duplicate cards in hand. TODO: fix?
			}
			
			//update player score
			if(options.highScoreWins()){
				winner.setScore(winner.getScore() + player_score);
				updateScore(winner.getName(), winner.getScore()); //TODO somewhat redundant.. can update all scores at once afterwards
			} else {
				current.setScore(current.getScore() + player_score);
				updateScore(current.getName(), current.getScore());
			}
			
			//printf("\nThe total number of points given for Player %d's hand is: %d\n", current.getName(), player_score);
			current = players.nextPlayer();
		}
		
		Player max_player = getMaxPlayer();
		boolean isGameOver = max_player.getScore() >= options.getGoalScore();
		
		//update game state
		if(isGameOver)
			game_state = END_GAME;
		else
			sendEndRoundMessage(winner, isGameOver); 
	}
	
	private void sendEndRoundMessage(Player winner, boolean isGameOver){
		if(winner == null)
			throw new IllegalArgumentException("winner must not be null");
		
		Bundle b = new Bundle();
		
		//set type
		b.putInt(GameActivity.TYPE_KEY, GameActivity.END_ROUND_DIALOG);	
		
		//set game/round over identifier
		b.putBoolean(GameActivity.GAME_OVER_KEY,isGameOver); 
		
		//set winner
		b.putString(GameActivity.WINNER_KEY, playerToString(winner));
		
		//set arrays of player strings and their scores
		String[] playerList = new String[options.getNumPlayers()];
		int[] scoreList = new int[options.getNumPlayers()];
		players.reset(human);
		Player current = players.getCurrent();
		for(int i=0; i<options.getNumPlayers(); i++){
			playerList[i] = playerToString(current);
			scoreList[i] = current.getScore();
			current = players.nextPlayer();
		}
		b.putStringArray(GameActivity.PLAYER_LIST_KEY, playerList);
		b.putIntArray(GameActivity.SCORE_LIST_KEY, scoreList);
		
		sendMessage(b);
		
		//update state
		game_state = PLAYER_TURN;
	}
	
	private Player getMaxPlayer(){
		int max_score = 0;
		Player max_player = null;
		Player current;
		int current_score = 0;
		for(int i=0; i<options.getNumPlayers(); i++){
			current = players.getCurrent();
			current_score = current.getScore();
			if(current_score > max_score){
				max_score = current_score;
				max_player = current;
			}
			players.nextPlayer();
		}
		
		return max_player;
	}
	
	private Player getMinPlayer(){
		int min_score = options.getGoalScore();
		Player min_player = null;
		Player current;
		int current_score = 0;
		for(int i=0; i<options.getNumPlayers(); i++){
			current = players.getCurrent();
			current_score = current.getScore();
			if(current_score < min_score){
				min_score = current_score;
				min_player = current;
			}
			players.nextPlayer();
		}
		return min_player;
	}
	
	private void endGame(){
		Player winner;
		if(options.highScoreWins())
			winner = getMaxPlayer();
		else
			winner = getMinPlayer(); 

		sendEndRoundMessage(winner, true);
	}
	
	@Override
	public void run(){
		while(running){
			synchronized(paused){
				while(paused){
					try {
						paused.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			
			switch(game_state){
			case NEW_GAME:
				newGame();
				break;
				
			case NEW_ROUND:
				newRound();
				break;
				
			case CPU_TURN:
				cpuTurn();
				break;
				
			case PLAYER_TURN:
				break;
				
			case END_ROUND:
				endRound();
				break;
				
			case END_GAME:
				endGame();
				break;
			}
		}
	}
	
	private void logEvent(Card card, String player, String event){
		int color = -1;
		int value = -1;
		if(card != null){
			color = card.getColor();
			value = card.getValue();
		}
		
		Bundle b = new Bundle();
		b.putInt(GameActivity.TYPE_KEY, GameActivity.EVENT_LOG_MSG);
		b.putInt(GameActivity.COLOR_KEY, color);
		b.putInt(GameActivity.VALUE_KEY, value);
		b.putString(GameActivity.PLAYER_KEY, player);
		b.putString(GameActivity.EVENT_KEY, event);
		
		sendMessage(b);
	}
	
	private void sendMessage(Bundle bundle){
		try{
			Message msg = new Message();
			msg.setData(bundle);
			
			handler.sendMessage(msg);
		} catch (Exception e){
			Log.v("Error", e.toString());
		}
	}
	
	public void endGameDialog(){
		game_state = NEW_GAME;
	}
	
	public GameOptions getOptions(){ return options; }
	public GameData getData(){ return data; }

	public void newGameDialog() {
		game_state = NEW_GAME;
		
	}

	public void newRoundDialog() {
		game_state = NEW_ROUND;
	}
	
	private void toast(String msg){
		if(msg == null)
			throw new IllegalArgumentException("msg must not be null");
		
		Bundle b = new Bundle();
		b.putInt(GameActivity.TYPE_KEY, GameActivity.TOAST_MSG);
		b.putString(GameActivity.EVENT_KEY, msg);
		
		sendMessage(b);
	}
}