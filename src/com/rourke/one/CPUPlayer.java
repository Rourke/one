package com.rourke.one;

import java.util.Random;

public class CPUPlayer extends Player{
	public CPUPlayer(GameData data_in, GameOptions options_in, int name_in){
		super(data_in, options_in, name_in);
	}
	
	public Card play(Card card_in_play){
		if(card_in_play == null)
			throw new IllegalArgumentException("card must not be null");
		
		Card card_to_play = null;
		int color = card_in_play.getColor();
		int value = card_in_play.getValue();
		boolean found = false;
		
		//attempt to play higher point cards of the same color first
		for(int i=data.getNumValues()-1; i>value && !found; i--){
			card_to_play = hand.remove(color, i);
			if(card_to_play != null)
				found = true;
		}

		//attempt to play same value card with highest number of cards in the suit
		int max = 0;
		int max_color = -1;
		//find suit with most cards and same value
		for(int i=0; i<options.getNumColors() && !found && value != data.getPlaceholderId(); i++){
			if(hand.cardInHand(i, value) && hand.numCardsOfColor(i) > max){
				max = hand.numCardsOfColor(i);
				max_color = i;
			}
		}
		//pick card with max number of cards in suit
		if(max > 0 && !found){
			found = true;
			card_to_play = hand.remove(max_color, value);
		}

		//attempt to play any remaining cards with the same color
		for(int i=value-1; i>=0 && !found; i--){
			card_to_play = hand.remove(color, i);
			if(card_to_play != null)
				found = true;
		}

		//attempt to play wild draw 4
		if(!found && options.wild4Enabled()){
			card_to_play = hand.remove(data.getWildColorId(), data.getWild4Id());
			if(card_to_play != null)
				found = true;
		}

		//attempt to play wild
		if(!found && options.wildEnabled()){
			card_to_play = hand.remove(data.getWildColorId(), data.getWildId());
			if(card_to_play != null) 
				found = true;
		}
		return card_to_play;
	}
	
	public int chooseWildColor(){
		int max = 0;
		int color = -1;
		int temp;

		//choose color with the most number of cards in hand
		for(int i=0; i<options.getNumColors(); i++){
			temp = hand.numCardsOfColor(i);
			if(temp > max){
				max = temp;
				color = i;
			}
		}

		//no colored cards found. choose a random color
		if(color < 0){  
			Random gen = new Random();
			color = gen.nextInt(options.getNumColors());
		}

		return color;
	}
	
	public boolean playDrawnCard(){ return true; }
}