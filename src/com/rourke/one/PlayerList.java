package com.rourke.one;

public class PlayerList{
	private class Node{
		Player value;
		Node clockwise;
		Node counter_clockwise;
	}
	
	private Node current;
	private boolean isClockwise;
	private int total_players;
	
	public PlayerList(){
		isClockwise = true;
		current = null;
		total_players = 0;
	}
	
	public void addPlayer(Player new_player){
		if(new_player == null)
			throw new IllegalArgumentException("player must not be null");
		
		Node node = new Node();
		node.value = new_player;
		
		if(current == null){
			current = node;
			node.clockwise = node;
			node.counter_clockwise = node;
		} else {
			node.clockwise = current;
			node.counter_clockwise = current.counter_clockwise;
			current.counter_clockwise.clockwise = node;
			current.counter_clockwise = node;
			//current = node;
		}
		
		total_players++;
	}
	
	public Player nextPlayer(){
		if(isClockwise)
			current = current.clockwise;
		else
			current = current.counter_clockwise;
		return current.value;
	}
	
	public void reverse(){
		isClockwise = !isClockwise;
	}
	
	public void reset(Player player){
		if(player == null)
			throw new IllegalArgumentException("player must not be null");
		
		for(int i=0; i<total_players && getCurrent() != player; i++)
			nextPlayer();
		
		if(getCurrent() != player)
			throw new IllegalArgumentException("Player not found in list");
		
		isClockwise = true;
	}
	
	public Player getCurrent(){ return current.value; }
	public boolean isClockwise(){ return isClockwise; }
}