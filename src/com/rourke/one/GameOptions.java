package com.rourke.one;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.PreferenceManager;

public class GameOptions implements Parcelable {
	private int colors;
	private int players;
	private int numbers;
	private int goal_score;
	private boolean reverse_enabled;
	private boolean draw2_enabled;
	private boolean skip_enabled;
	private boolean wild_enabled;
	private boolean wild4_enabled;
	private boolean high_score_wins;
	
	static final String REVERSE_KEY = "reverse";
	static final String DRAW2_KEY = "draw2";
	static final String SKIP_KEY  = "skip";
	static final String WILD_KEY = "wild";
	static final String WILD4_KEY = "wild4";
	static final String HIGH_SCORE_KEY = "high score";
	
	public GameOptions(){
		colors = 4;
		players = 4;
		numbers = 10;
		goal_score = 500;
		draw2_enabled = true;
		reverse_enabled = true;
		skip_enabled = true;
		wild_enabled = true;
		wild4_enabled = true;
		high_score_wins = true;
	}
	
	public GameOptions(Context context){
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		
		colors = Integer.parseInt(pref.getString(PrefActivity.KEY_PREF_NUM_COLORS, "4"));
		players = Integer.parseInt(pref.getString(PrefActivity.KEY_PREF_NUM_PLAYERS, "4"));
		numbers = Integer.parseInt(pref.getString(PrefActivity.KEY_PREF_NUM_NUMBERS, "10"));
		goal_score = Integer.parseInt(pref.getString(PrefActivity.KEY_PREF_PLAY_TO, "500"));
		draw2_enabled = pref.getBoolean(PrefActivity.KEY_PREF_DRAW2, true);
		reverse_enabled = pref.getBoolean(PrefActivity.KEY_PREF_REVERSE, true);
		skip_enabled = pref.getBoolean(PrefActivity.KEY_PREF_SKIP, true);
		wild_enabled = pref.getBoolean(PrefActivity.KEY_PREF_WILD, true);
		wild4_enabled = pref.getBoolean(PrefActivity.KEY_PREF_WILD4, true);
		high_score_wins = pref.getBoolean(PrefActivity.KEY_PREF_HIGH_WINS, true);
	}
	
	public int getNumColors(){ return colors; }
	public int getNumPlayers(){ return players; }
	public int getNumNumbers(){ return numbers; }
	public int getGoalScore(){ return goal_score; }
	public boolean reverseEnabled(){ return reverse_enabled; }
	public boolean draw2Enabled(){ return draw2_enabled; }
	public boolean skipEnabled(){ return skip_enabled; }
	public boolean wildEnabled(){ return wild_enabled; }
	public boolean wild4Enabled(){ return wild4_enabled; }
	public boolean highScoreWins(){ return high_score_wins; }

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(colors);
		out.writeInt(players);
		out.writeInt(numbers);
		out.writeInt(goal_score);
		
		Bundle b = new Bundle();
		b.putBoolean(REVERSE_KEY, reverse_enabled);
		b.putBoolean(DRAW2_KEY, draw2_enabled);
		b.putBoolean(SKIP_KEY, skip_enabled);
		b.putBoolean(WILD_KEY, wild_enabled);
		b.putBoolean(WILD4_KEY, wild4_enabled);
		b.putBoolean(HIGH_SCORE_KEY, high_score_wins);
		
		out.writeBundle(b);
	}
	
	public static final Parcelable.Creator<GameOptions> CREATOR
		= new Parcelable.Creator<GameOptions>() {

			@Override
			public GameOptions createFromParcel(Parcel in) {
				return new GameOptions(in);
			}

			@Override
			public GameOptions[] newArray(int size) {
				// TODO Auto-generated method stub
				return null;
			}
	};
	
	private GameOptions(Parcel in) {
		colors = in.readInt();
		players = in.readInt();
		numbers = in.readInt();
		goal_score = in.readInt();
		
		Bundle b = in.readBundle();
		reverse_enabled = b.getBoolean(REVERSE_KEY);
		draw2_enabled = b.getBoolean(DRAW2_KEY);
		skip_enabled = b.getBoolean(SKIP_KEY);
		wild_enabled = b.getBoolean(WILD_KEY);
		wild4_enabled = b.getBoolean(WILD4_KEY);
		high_score_wins = b.getBoolean(HIGH_SCORE_KEY);
	}
}