package com.rourke.one;

public class Card{
	private int color;
	private int value;
	
	public Card(int color_in, int value_in){
		this.value = value_in;
		this.color = color_in;
	}
	
	public int getValue(){
		return value;
	}
	
	public int getColor(){
		return color;
	}
}