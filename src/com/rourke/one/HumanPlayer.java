package com.rourke.one;

public class HumanPlayer extends Player{
	public HumanPlayer(GameData data_in, GameOptions options_in, int name_in){
		super(data_in, options_in, name_in);
	}
	
	public Card play(Card card_in_play){
		return null;
	}
	
	public int chooseWildColor(){
		return 0;
	}
	
	public boolean playDrawnCard(){
		return true;
	}
}