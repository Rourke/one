package com.rourke.one;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class GameActivity extends Activity {
	public static final int PLAYER_BAR_MSG = 0;
	public static final int TOP_CARD_MSG = 1;
	public static final int EVENT_LOG_MSG = 2;
	public static final int HAND_GRID_MSG = 3;
	public static final int DRAWN_CARD_DIALOG = 4;
	public static final int WILD_DIALOG = 5;
	public static final int END_ROUND_DIALOG = 6;
	public static final int TOAST_MSG = 7;
	public static final String TYPE_KEY = "type";
	public static final String GAME_OVER_KEY = "subtype";
	public static final String COLOR_KEY = "color";
	public static final String VALUE_KEY = "value";
	public static final String PLAYER_KEY = "player";
	public static final String EVENT_KEY = "event";
	public static final String SCORE_KEY = "score";
	public static final String NUM_CARDS_KEY = "num cards";
	public static final String CLOCKWISE_KEY = "clockwise";
	public static final String WINNER_KEY = "winner";
	public static final String PLAYER_LIST_KEY = "player list";
	public static final String SCORE_LIST_KEY = "score list";
	public static final String OPTIONS_KEY = "options";
	public static final String DATA_KEY = "data";
	
	static Context context;
	static LinearLayout logView;
	static PlayerBar playerBar;
	static CardView topCard;
	static GridView gridView;
	static Game game;
	Thread gameThread;
	static Handler handler = new Handler(){
		@Override
		public void handleMessage(Message msg){
			Bundle b = msg.getData();
			
			if(b.containsKey(TYPE_KEY)){
				switch(b.getInt(TYPE_KEY)){
				case PLAYER_BAR_MSG:
					updatePlayerBar(b);
					break;
					
				case TOP_CARD_MSG:
					updateTopCard(b);
					break;
					
				case EVENT_LOG_MSG:
					logEvent(b);
					break;
					
				case HAND_GRID_MSG:
					notifyGrid();
					break;
					
				case DRAWN_CARD_DIALOG:
					drawnCardDialog(b);
					break;
					
				case WILD_DIALOG:
					chooseColorDialog(b);
					break;
					
				case END_ROUND_DIALOG:
					endRoundDialog(b);
					break;
					
				case TOAST_MSG:
					toast(b);
					
				default:
						
				}
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);
		
		context = this;
		//init UI elements
		playerBar = new PlayerBar(this); //TODO change PlayerBar to use any linearlayout
		logView = (LinearLayout)findViewById(R.id.logview);
		gridView = (GridView)findViewById(R.id.gridview);
		topCard = (CardView)findViewById(R.id.top_card);
		
		//init game
		game = new Game(new GameOptions(this), context, playerBar, gridView, handler, topCard);
		game.setRunning(true);
		gameThread = new Thread(game);
		gameThread.start();
	}

	@Override 
	protected void onResume(){
		super.onResume();
		
		try{
		//game = new GameThread(new GameOptions(this));
		//game.setRunning(true);
		//game.run();
			game.setPaused(false);
		} catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		
		game.setPaused(true);
		
		/*
		boolean retry = true;
		game.setRunning(false);
		while(retry){
			try{
				game.join();
				retry = false;
			} catch(InterruptedException e){
				//try again
			}
		}
		*/
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_game, menu);
		return true; 
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public static void logEvent(Bundle b){
		if(b == null)
			throw new IllegalArgumentException("bundle must not be null");
		if(!(b.containsKey(COLOR_KEY) && b.containsKey(VALUE_KEY)))
			throw new IllegalArgumentException("must set at least color and value for logged event");
		
		int color = b.getInt(COLOR_KEY);
		int value = b.getInt(VALUE_KEY);
		
		String player = null;
		if(b.containsKey(PLAYER_KEY))
			player = b.getString(PLAYER_KEY);
		
		String event = null;
		if(b.containsKey(EVENT_KEY))
			event = b.getString(EVENT_KEY);
		
		if(color<0 && value<0 && player==null && event==null)//TODO: make a separate key in message for this instead?
			logView.removeAllViews();
		else{
			CardView cardView = new CardView(context, game.getOptions(), game.getData());
			cardView.setCard(color, value);
			cardView.setPlayer(player);
			cardView.setEvent(event);
			cardView.setPadding(1, 1, 1, 1);
			
			logView.addView(cardView, 0, new LinearLayout.LayoutParams(85, 85));
			
			HorizontalScrollView hsv = (HorizontalScrollView)logView.getParent();
			hsv.fullScroll(HorizontalScrollView.FOCUS_LEFT);
			
			System.out.println(player+" "+event+" "+Integer.toString(color)+" "+Integer.toString(value));
		}
	}

	public void reneg(View view){
		game.reneg();
	}
	
	public void endGameDialog(){
		game.endGameDialog();
	}
	
	private static void updateTopCard(Bundle b){
		if(b == null)
			throw new IllegalArgumentException("bundle must not be null");
		if(!(b.containsKey(COLOR_KEY) && b.containsKey(VALUE_KEY)))
			throw new IllegalArgumentException("must set both color and value of top card");
		
		int color = b.getInt(COLOR_KEY);
		int value = b.getInt(VALUE_KEY);
		
		topCard.setCard(color, value);
	}
	
	private static void notifyGrid(){
		CardAdapter adapter = (CardAdapter)gridView.getAdapter();
		adapter.notifyDataSetChanged();
	}
	
	private static void updatePlayerBar(Bundle b){
		if(b == null)
			throw new IllegalArgumentException("bundle must not be null");
		
		if(b.containsKey(PLAYER_KEY)){
			int player = b.getInt(PLAYER_KEY);
	
			if(b.containsKey(SCORE_KEY)){
				int score = b.getInt(SCORE_KEY);
				playerBar.updateScore(player, score);
			}
			if(b.containsKey(NUM_CARDS_KEY)){
				int numCards = b.getInt(NUM_CARDS_KEY);
				playerBar.updateNumCards(player, numCards);
			}
		}
		
		if(b.containsKey(CLOCKWISE_KEY)){
			boolean clockwise = b.getBoolean(CLOCKWISE_KEY);
			playerBar.setClockwise(clockwise);
		}
	}
	
	private static void drawnCardDialog(Bundle b){
		if(b == null)
			throw new IllegalArgumentException("bundle must not be null");
		if(!(b.containsKey(COLOR_KEY) && b.containsKey(VALUE_KEY)))
			throw new IllegalArgumentException("drawn card must have both a color and value");
		if(!(b.containsKey(OPTIONS_KEY) && b.containsKey(DATA_KEY)))
			throw new IllegalArgumentException("both GameOptions and GameData must be present");
		
		CardView cardView = new CardView(context, (GameOptions)b.getParcelable(OPTIONS_KEY), (GameData)b.getParcelable(DATA_KEY));
		final int color = b.getInt(COLOR_KEY);
		final int value = b.getInt(VALUE_KEY);
		cardView.setCard(color, value);
		cardView.setLayoutParams(new LinearLayout.LayoutParams(90, 90));
		
		AlertDialog dialog = new AlertDialog.Builder(context)
			.setTitle("Play drawn card?")
			.setView(cardView)
			.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					game.playDrawnCard(true, color, value);
				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					game.playDrawnCard(false, color, value);
				}
			})
			.setCancelable(false)
			.create();
		
		dialog.show();
	}
	
	private static void chooseColorDialog(Bundle b){
		if(b == null)
			throw new IllegalArgumentException("bundle must not be null");
		if(!b.containsKey(OPTIONS_KEY))
			throw new IllegalArgumentException("bundle must contain GameOptions");
		
		GameOptions options = b.getParcelable(OPTIONS_KEY);
		//GameData data = b.getParcelable(DATA_KEY);
		
		String[] all_items = context.getResources().getStringArray(R.array.colors);
		//only provide options for colors that are enabled
		String[] items = new String[all_items.length];
		for(int i=0; i<options.getNumColors(); i++)
			items[i] = all_items[i];
		
		AlertDialog dialog = new AlertDialog.Builder(context)
			.setTitle("Choose a color")
			.setItems(items, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					game.chooseWildColor(which);
				}
			})
			.setCancelable(false)
			.create();
		
		dialog.show();
	}
	
	private static void endRoundDialog(Bundle b){
		if(b == null)
			throw new IllegalArgumentException("bundle must not be null");
		if(!b.containsKey(GAME_OVER_KEY))
			throw new IllegalArgumentException("bundle must contain game over status");
		if(!(b.containsKey(WINNER_KEY) && b.containsKey(PLAYER_LIST_KEY) && b.containsKey(SCORE_LIST_KEY)))
			throw new IllegalArgumentException("bundle must contain a winner, list of players and their scores");
		
		final boolean isGameOver = b.getBoolean(GAME_OVER_KEY);
		String winner = b.getString(WINNER_KEY);
		String[] players = b.getStringArray(PLAYER_LIST_KEY);
		int[] scores = b.getIntArray(SCORE_LIST_KEY);
		
		if(players.length != scores.length)
			throw new IllegalArgumentException("there must be the same number of players as there are scores");
		
		String scoreDisplay = "";
		for(int i=0; i<players.length; i++)
			scoreDisplay += players[i] + ":\t\t" + Integer.toString(scores[i]) + "\n";
		
		String title = winner + " ";
		if(isGameOver){
			title += "wins the game!";
			scoreDisplay += "Play again?";
		} else
			title += "wins the round";
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context)
			.setTitle(title)
			.setMessage(scoreDisplay)
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if(isGameOver)
						game.newGameDialog();
					else
						game.newRoundDialog();
				}
			})
			.setCancelable(false);
		
		if(isGameOver)
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = new Intent(context, MainActivity.class);
					context.startActivity(intent);
				}
			});
		
		AlertDialog dialog = builder.create();
		
		dialog.show();
	}
	
	private static void toast(Bundle b) {
		if(b == null)
			throw new IllegalArgumentException("bundle must not be null");
		if(!b.containsKey(EVENT_KEY))
			throw new IllegalArgumentException("bundle must contain an event to toast");
		
		String text = b.getString(EVENT_KEY);
		int duration = Toast.LENGTH_SHORT;
		
		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}
}
