package com.rourke.one;

import java.util.Random;

public class Deck{
	private class Node{
		Card value;
		Node next;
	}
	
	private Node top;
	private int total_cards;
	
	public Deck(){
		top = null;
		total_cards = 0;
	}
	
	public Card pop(){
		Card holder = null;
		if(top != null){
			holder = top.value;
			top = top.next;
			total_cards--;
		} 
		return holder;
	}
	
	public void push(Card card){
		if(card == null)
			throw new NullPointerException();
		Node new_node = new Node();
		new_node.value = card;
		new_node.next = top;
		top = new_node;
		total_cards++;
	}
	
	public Card peek(){
		Card top_card = null;
		if(top != null)
			top_card = top.value;
		return top_card;
	}
	
	public void shuffle(){
		int array_length = 3*total_cards;
		Card array[] = new Card[array_length];
		Card temp;
		Random gen = new Random();
		
		for(int i=0; i<array_length; i++)
			array[i] = null;
		
		//remove cards from deck and mix them
		while( (temp=pop()) != null){
			int i = gen.nextInt(array_length);
			while(true){
				if(array[i] == null){
					array[i] = temp;
					break;
				}
				i = (i+1) % array_length;
			}
		}
		
		//insert cards back into deck
		for(int i=0; i<array_length; i++){
			if(array[i] != null)
				push(array[i]);
		}
	}
}

/*
public class Deck{
	private Stack deck;
	
	public Card draw(){
		return this.deck.pop();
	}
	
	public void discard(Card card){
		this.deck.push(card);
	}
	
	public void shuffle(){
		Card[] array = new Card[500];
		Card temp;
		Random rand = new Random();
		while((temp=this.draw()) != null){
			int i = rand.nextInt()%array.length;
			while(true){
				if(array[i] == null){
					array[i] = temp;
					break;
				}
				i=++i%array.length;
			}
		}
		for(int i=0; i<array.length; i++){
			if(array[i] != null)
				this.discard(array[i]);
		}
	}
	
	private class Stack{
		private Node top = null;
		
		public void push(Card card){
			Node newNode = new Node(card, top);
			this.top = newNode;
		}
		
		public Card pop(){
			Card topCard = null;
			if(this.top != null){
				topCard = this.top.getValue();
				this.top = top.getNext();
			}
			return topCard;
		}
		
		private class Node{
			private Node next;
			private Card value;
			
			public Node(Card val, Node next){
				this.value = val;
				this.next = next;
			}
			public Card getValue(){
				return this.value;
			}
			
			public Node getNext(){
				return this.next;
			}
		}
	}
}
*/