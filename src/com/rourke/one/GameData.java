package com.rourke.one;

import android.os.Parcel;
import android.os.Parcelable;

public class GameData implements Parcelable {
	private	int values;
	private	int reverse_id;
	private	int draw2_id;
	private	int skip_id;
	private	int wild_id;
	private	int wild4_id;
	private	int wild_color_id;
	private	int placeholder_id;

	public GameData(GameOptions options){
		values = options.getNumNumbers();
		if(options.draw2Enabled())
			draw2_id = values++;
		if(options.reverseEnabled())
			reverse_id = values++;
		if(options.skipEnabled())
			skip_id = values++;
		if(options.wildEnabled())
			wild_id = values++;
		if(options.wild4Enabled())
			wild4_id = values++;
		if(options.wildEnabled() || options.wild4Enabled())
			wild_color_id = options.getNumColors();
		else 
			wild_color_id = -1;
		placeholder_id = -1;
	}
	
	public int getReverseId(){ return reverse_id; }
	public int getDraw2Id(){ return draw2_id; }
	public int getSkipId(){ return skip_id; }
	public int getWildId(){ return wild_id; }
	public int getWild4Id(){ return wild4_id; }
	public int getWildColorId(){ return wild_color_id; }
	public int getPlaceholderId(){ return placeholder_id; }
	public int getNumValues(){ return values; }

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(values);
		out.writeInt(reverse_id);
		out.writeInt(draw2_id);
		out.writeInt(skip_id);
		out.writeInt(wild_id);
		out.writeInt(wild4_id);
		out.writeInt(wild_color_id);
		out.writeInt(placeholder_id);
	}
	
	public static final Parcelable.Creator<GameData> CREATOR
		= new Parcelable.Creator<GameData>() {
		public GameData createFromParcel(Parcel in){
			return new GameData(in);
		}

		@Override
		public GameData[] newArray(int size) { //would never want to use this..
			return null; 
		}
	};
	
	private GameData(Parcel in){
		values = in.readInt();
		reverse_id = in.readInt();
		draw2_id = in.readInt();
		skip_id = in.readInt();
		wild_id = in.readInt();
		wild4_id = in.readInt();
		wild_color_id = in.readInt();
		placeholder_id = in.readInt();
	}
}