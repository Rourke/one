package com.rourke.one;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class PlayerView extends View {
	String playerName;
	int numCards;
	int score;
	Paint mTextPaint;
	Paint rectPaint;
	RectF viewBounds;
	
	public PlayerView(Context context){
		super(context);
		
		init();
	}

	public PlayerView(Context context, AttributeSet attrs) {
		super(context, attrs);

		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.PlayerView, 0, 0);
		
		try{
			numCards = a.getInteger(R.styleable.PlayerView_num_cards, 0);
			score = a.getInteger(R.styleable.PlayerView_score, 0);
			playerName = a.getString(R.styleable.PlayerView_name);
		} finally {
			a.recycle();
		}
		
		init();
	}

	private void init(){
		mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mTextPaint.setColor(Color.WHITE);
		
		rectPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		rectPaint.setColor(Color.BLACK);
		rectPaint.setStyle(Paint.Style.FILL);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		
		// Account for padding
        float xpad = (float) (getPaddingLeft() + getPaddingRight());
        float ypad = (float) (getPaddingTop() + getPaddingBottom());

        float ww = (float) w - xpad;
        float hh = (float) h - ypad;

        viewBounds = new RectF(
                0.0f,
                0.0f,
                ww,
                hh);
        viewBounds.offsetTo(getPaddingLeft(), getPaddingTop());
	}
	
	@Override
	protected void onDraw(Canvas canvas){
		super.onDraw(canvas);
		
		canvas.drawRect(viewBounds, rectPaint);
		
		canvas.drawText(playerName, viewBounds.left, viewBounds.top+10, mTextPaint);
		canvas.drawText(Integer.toString(numCards), viewBounds.left, viewBounds.bottom/2, mTextPaint);
		canvas.drawText(Integer.toString(score), viewBounds.left, viewBounds.bottom, mTextPaint);
	}
	
	public void setName(String name){
		playerName = name;
		this.invalidate();
		this.requestLayout();
	}
	
	public String getName(){
		return playerName;
	}
	
	public void setNumCards(int num){
		numCards = num;
		this.invalidate();
		this.requestLayout();
	}
	
	public void setScore(int score){
		this.score = score;
		this.invalidate();
		this.requestLayout();
	}
}
