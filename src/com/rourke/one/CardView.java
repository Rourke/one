package com.rourke.one;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class CardView extends View {
	GameOptions options;
	GameData data;
	int color;
	int value;
	Paint mTextPaint;
	Paint rectPaint;
	Paint cardPaint;
	RectF cardBounds;
	RectF rectBounds;
	int[] colors = { Color.RED, Color.YELLOW, Color.BLUE, Color.GREEN };
	int wildColor = Color.BLACK; 
	String player;
	String event;
	Resources res;
	Bitmap bitmap;
	float hOverWRatio;
	float xOffsetRatio;
	float yOffsetRatio;
	
	static final int numberCards[] = { R.drawable.card_0, R.drawable.card_1, R.drawable.card_2, R.drawable.card_3,
			R.drawable.card_4, R.drawable.card_5, R.drawable.card_6, R.drawable.card_7, R.drawable.card_8, R.drawable.card_9};
	static final int draw2Card = R.drawable.card_draw2;
	static final int reverseCard = R.drawable.card_reverse;
	static final int skipCard = R.drawable.card_skip;
	static final int wildCard = R.drawable.card_wild;
	static final int wild4Card = R.drawable.card_wild4;
	static final int blankCard = R.drawable.card_blank;
	
	public CardView(Context context, GameOptions options, GameData data){
		super(context);
		
		if(options == null || data == null)
			throw new IllegalArgumentException("params must not be null");
		
		this.options = options;
		this.data = data;
		
		color = data.getWildColorId();
		value = data.getPlaceholderId();
		
		init();
	}
	
	public CardView(Context context, AttributeSet attrs) {
		super(context, attrs);

		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CardView, 0, 0);
		
		try{
			color = a.getInteger(R.styleable.CardView_color, -1);
			value = a.getInteger(R.styleable.CardView_value, -1);
			player = a.getString(R.styleable.CardView_player);
			event = a.getString(R.styleable.CardView_event);
		} finally {
			a.recycle();
		}
		
		init();
	}
	
	private void init(){
		mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mTextPaint.setColor(Color.WHITE); 
		mTextPaint.setShadowLayer(1, 0, 0, Color.BLACK);
		
		rectPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		rectPaint.setStyle(Paint.Style.FILL);
		
		cardPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		
		res = getContext().getResources();
		updateBitmap();
	}
	
	private void updateBitmap(){
		int bitmap_id = blankCard;
		
		if(options != null && data != null){ //hack for xml defined cardview.. TODO: fix?
			if(value == data.getWild4Id() && options.wild4Enabled())
				bitmap_id = wild4Card;
			else if(value == data.getWildId() && options.wildEnabled())
				bitmap_id = wildCard;
			else if(value == data.getSkipId() && options.skipEnabled())
				bitmap_id = skipCard;
			else if(value == data.getReverseId() && options.reverseEnabled())
				bitmap_id = reverseCard;
			else if(value == data.getDraw2Id() && options.draw2Enabled())
				bitmap_id = draw2Card;
			else if(value >= 0 && value < options.getNumNumbers())
				bitmap_id = numberCards[value];
		}
		
		bitmap = BitmapFactory.decodeResource(res, bitmap_id);
		
		calcRatio();
		calcXRatio();
		calcYRatio();
	}
	
	private void calcRatio(){
		hOverWRatio = (float)bitmap.getHeight()/(float)bitmap.getWidth();
	}
	
	private void calcXRatio(){
		int y = bitmap.getHeight() / 3;
		
		int x=0;
		while(bitmap.getPixel(x, y) == Color.TRANSPARENT) //if edge of bitmap is transparent
			x++;
		while(bitmap.getPixel(x, y) != Color.TRANSPARENT) //find inside of card border
			x++;
		
		xOffsetRatio = (float)--x / (float)bitmap.getWidth(); 
	}
	
	private void calcYRatio(){
		int x = bitmap.getWidth() / 2;
		
		int y=0;
		while(bitmap.getPixel(x, y) == Color.TRANSPARENT)
			y++;
		while(bitmap.getPixel(x, y) != Color.TRANSPARENT)
			y++;
		
		yOffsetRatio = (float)--y / (float)bitmap.getHeight();
	}
	
	public float getRatio(){ return hOverWRatio; }

	public int getColor(){
		return color;
	}
	
	public int getValue(){
		return value;
	}
	
	public void setColor(int color){
		this.color = color;
		invalidate();
		requestLayout();
	}
	
	public void setValue(int value){
		int temp = this.value;
		this.value = value;
		
		if(temp != value) //only load bitmap if value changes
			updateBitmap();
		
		invalidate();
		requestLayout();
	}
	
	public void setCard(int color, int value){
		setColor(color);
		setValue(value);
	}
	
	public void setPlayer(String player){
		this.player = player;
		invalidate();
		requestLayout();
	}
	
	public void setEvent(String event){
		this.event = event;
		invalidate();
		requestLayout();
	}
	
	public void setOptions(GameOptions options){
		this.options = options;
	}
	
	public void setData(GameData data){
		this.data = data;
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		
		// Account for padding
        float xpad = (float) (getPaddingLeft() + getPaddingRight());
        float ypad = (float) (getPaddingTop() + getPaddingBottom());

        float ww = (float) w - xpad;
        float hh = (float) h - ypad;

        cardBounds = new RectF(
                0.0f,
                0.0f,
                ww,
                hh);
        cardBounds.offsetTo(getPaddingLeft(), getPaddingTop());
        
        int rectPaddingX = (int)(ww*xOffsetRatio);
        int rectPaddingY = (int)(hh*yOffsetRatio);
        rectBounds = new RectF(
        		0.0f,
        		0.0f,
        		ww-(rectPaddingX*2),
        		hh-(rectPaddingY*2));
        rectBounds.offsetTo(rectPaddingX+getPaddingLeft(), rectPaddingY+getPaddingTop());
	}
	
	@Override
	protected void onDraw(Canvas canvas){
		super.onDraw(canvas);
		
		if(this.color >= 0 && this.color < colors.length)
			rectPaint.setColor(colors[color]);
		else
			rectPaint.setColor(wildColor);
		canvas.drawRect(rectBounds, rectPaint);
		
		canvas.drawBitmap(bitmap, null, cardBounds, cardPaint);
		
		if(player != null) canvas.drawText(player, cardBounds.left, cardBounds.top+10, mTextPaint);
		//canvas.drawText(Integer.toString(value), cardBounds.left, cardBounds.bottom/2, mTextPaint);
		if(event != null) canvas.drawText(event, cardBounds.left, cardBounds.bottom, mTextPaint);
		
	}
}
