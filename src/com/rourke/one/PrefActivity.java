package com.rourke.one;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class PrefActivity extends PreferenceActivity {
	
	public static final String KEY_PREF_NUM_PLAYERS = "pref_numPlayers";
	public static final String KEY_PREF_PLAY_TO = "pref_playToScore";
	public static final String KEY_PREF_HIGH_WINS = "pref_highScoreWins";
	public static final String KEY_PREF_NUM_COLORS = "pref_numColors";
	public static final String KEY_PREF_NUM_NUMBERS = "pref_numNumbers";
	public static final String KEY_PREF_DRAW2 = "pref_draw2Enabled";
	public static final String KEY_PREF_REVERSE = "pref_reverseEnabled";
	public static final String KEY_PREF_SKIP = "pref_skipEnabled";
	public static final String KEY_PREF_WILD = "pref_wildEnabled";
	public static final String KEY_PREF_WILD4 = "pref_wild4Enabled";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
	}

}
