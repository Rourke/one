package com.rourke.one;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

public class CardAdapter extends BaseAdapter {
	private Context mContext;
	Player player;
	GameOptions options;
	GameData data;
	
	public CardAdapter(Context c, Player p, GameOptions options, GameData data){
		mContext = c;
		player = p;
		this.options = options;
		this.data = data;
	}

	@Override
	public int getCount() {
		return player.getNumCardsInHand();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		CardView cardView;
		Card card = null;
		
		if(convertView == null){
			cardView = new CardView(mContext, options, data);
			
			cardView.setPadding(1, 1, 1, 1);
		} else
			cardView = (CardView) convertView;
		
		player.resetIter();
		for(int i=0; i<=position; i++){
			card = player.nextIter();
		}
		
		cardView.setCard(card.getColor(), card.getValue());
		cardView.setLayoutParams(new GridView.LayoutParams(100, (int)(100*cardView.getRatio())));
		
		return cardView;
		
		/*
		 * CardView cardView;
		Card card = null;
		
		if(convertView == null){
			cardView = new CardView(mContext, options, data);
			cardView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			//cardView.setLayoutParams(parent.);
			//cardView.setAdjustViewBounds(true)card;
			cardView.setPadding(1, 1, 1, 1);
		} else
			cardView = (CardView) convertView;
		
		player.resetIter();
		for(int i=0; i<=position; i++){
			card = player.nextIter();
		}
		
		cardView.setCard(card.getColor(), card.getValue());
		
		return cardView;
		 */
	}

}
